﻿// <copyright file="TestBase.cs" company="Rolosoft Ltd">
// � 2021, Rolosoft Ltd
// </copyright>

namespace Rsft.Util.Compression.Tests
{
    using System;
    using JetBrains.Annotations;
    using Xunit.Abstractions;

    /// <summary>
    /// Test Base.
    /// </summary>
    public abstract class TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        /// <param name="outHelper">The out helper.</param>
        /// <exception cref="ArgumentNullException">outHelper.</exception>
        protected TestBase([NotNull] ITestOutputHelper outHelper)
        {
            this.OutHelper = outHelper;
        }

        /// <summary>
        /// Gets the out helper.
        /// </summary>
        /// <value>
        /// The out helper.
        /// </value>
        protected ITestOutputHelper OutHelper { get; }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(long timerElapsed)
        {
            this.OutHelper.WriteLine($"Elapsed timer: {timerElapsed}ms");
        }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(TimeSpan timerElapsed)
        {
            this.OutHelper.WriteLine($"Elapsed timer: {timerElapsed}");
        }
    }
}