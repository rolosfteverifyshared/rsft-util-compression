﻿// <copyright file="CompressionTests.cs" company="Rolosoft Ltd">
// � 2021, Rolosoft Ltd
// </copyright>

// Copyright 2021 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Util.Compression.Tests.Integration
{
    using System.IO.Compression;
    using JetBrains.Annotations;
    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// The compression integration tests.
    /// </summary>
    public class CompressionTests : TestBase
    {
        /// <summary>
        ///     The test string.
        /// </summary>
        private const string TestString =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><MyRootElement value=\"MyValue\"><Person name=\"Fred Blogs\"></Person><Person name=\"John Smith\"></Person><Person name=\"John Doe\"></Person><Person name=\"Jane Doe\"></Person><Person name=\"Winnie The Pooh\"></Person><Person name=\"Captain America\"></Person><Person name=\"The Hulk\"></Person><Person name=\"Superman\"></Person><Person name=\"Bruce Banner\"></Person><Person name=\"Bruce Wayne\"></Person><Person name=\"Clark Kent\"></Person></MyRootElement>";

        /// <summary>
        /// Initializes a new instance of the <see cref="CompressionTests"/> class.
        /// </summary>
        /// <param name="outHelper">The out helper.</param>
        /// <exception cref="System.ArgumentNullException">outHelper</exception>
        public CompressionTests([NotNull] ITestOutputHelper outHelper)
                    : base(outHelper)
        {
        }

        /// <summary>
        /// The compress and uncompress_ string using deflate fastest_ expect match.
        /// </summary>
        [Fact]
        public void CompressAndUncompress_StringUsingDeflateFastest_ExpectMatch()
        {
            // arrange

            // act
            var compress = TestString.Compress(CompressionType.Deflate, CompressionLevel.Fastest);
            var decompress = compress.Decompress();

            // assert
            Assert.True(TestString.Length > compress.Length);
            Assert.Equal(TestString, decompress);
            this.OutHelper.WriteLine(@"Starting string data size={0} bytes", TestString.Length);
            this.OutHelper.WriteLine(@"Compressed data size={0} bytes", compress.Length);
            this.OutHelper.WriteLine(@"Uncompressed data size={0} bytes", decompress.Length);
            this.OutHelper.WriteLine(
                @"Compression Ratio={0}%",
                CalculateCompressionRatioPercent(TestString.Length, compress.Length).ToString("F"));
        }

        /// <summary>
        /// The compress and uncompress_ string using deflate optimal_ expect match.
        /// </summary>
        [Fact]
        public void CompressAndUncompress_StringUsingDeflateOptimal_ExpectMatch()
        {
            // arrange

            // act
            var compress = TestString.Compress();
            var decompress = compress.Decompress();

            // assert
            Assert.True(TestString.Length > compress.Length);
            Assert.Equal(TestString, decompress);
            this.OutHelper.WriteLine(@"Starting string data size={0} bytes", TestString.Length);
            this.OutHelper.WriteLine(@"Compressed data size={0} bytes", compress.Length);
            this.OutHelper.WriteLine(@"Uncompressed data size={0} bytes", decompress.Length);
            this.OutHelper.WriteLine(
                @"Compression Ratio={0}%",
                CalculateCompressionRatioPercent(TestString.Length, compress.Length).ToString("F"));
        }

        /// <summary>
        /// The compress and uncompress_ string using g zip fastest_ expect match.
        /// </summary>
        [Fact]
        public void CompressAndUncompress_StringUsingGZipFastest_ExpectMatch()
        {
            // arrange

            // act
            var compress = TestString.Compress(CompressionType.GZip, CompressionLevel.Fastest);
            var decompress = compress.Decompress(CompressionType.GZip);

            // assert
            Assert.True(TestString.Length > compress.Length);
            Assert.Equal(TestString, decompress);
            this.OutHelper.WriteLine(@"Starting string data size={0} bytes", TestString.Length);
            this.OutHelper.WriteLine(@"Compressed data size={0} bytes", compress.Length);
            this.OutHelper.WriteLine(@"Uncompressed data size={0} bytes", decompress.Length);
            this.OutHelper.WriteLine(
                @"Compression Ratio={0}%",
                CalculateCompressionRatioPercent(TestString.Length, compress.Length).ToString("F"));
        }

        /// <summary>
        /// The compress and uncompress_ string using g zip optimal_ expect match.
        /// </summary>
        [Fact]
        public void CompressAndUncompress_StringUsingGZipOptimal_ExpectMatch()
        {
            // arrange

            // act
            var compress = TestString.Compress(CompressionType.GZip);
            var decompress = compress.Decompress(CompressionType.GZip);

            // assert
            Assert.True(TestString.Length > compress.Length);
            Assert.Equal(TestString, decompress);
            this.OutHelper.WriteLine(@"Starting string data size={0} bytes", TestString.Length);
            this.OutHelper.WriteLine(@"Compressed data size={0} bytes", compress.Length);
            this.OutHelper.WriteLine(@"Uncompressed data size={0} bytes", decompress.Length);
            this.OutHelper.WriteLine(
                @"Compression Ratio={0}%",
                CalculateCompressionRatioPercent(TestString.Length, compress.Length).ToString("F"));
        }

        /// <summary>
        /// Calculates the compression ratio percent.
        /// </summary>
        /// <param name="uncompressedLength">
        /// Length of the uncompressed.
        /// </param>
        /// <param name="compressedLength">
        /// Length of the compressed.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        private static decimal CalculateCompressionRatioPercent(int uncompressedLength, int compressedLength)
        {
            decimal a = uncompressedLength;
            decimal b = compressedLength;

            decimal result = ((a - b) / uncompressedLength) * 100;

            return result;
        }
    }
}