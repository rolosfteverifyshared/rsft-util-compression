# Compression Utilities

## About
Compress and un-compress small amounts of in-memory data using convenient helper extensions.

## Features

 * Support compression types Deflate and GZip.
 * Compression levels of None, Optimal and Fastest.
 * Compress and un-compress string or byte[].

## Requirements

 * .NET framework 4.5 or higher.

## Installation
Please install via [NuGet](https://www.nuget.org).

```
install-package Rsft.Util.Compression
```

## Note on Compression Methods
By default, Deflate compression is used. Deflate generally (as shown in the integration tests in this software) provides better compression ratios than GZip.

For the test scenario data in our test, results are as follows:

| Compression Type | Compression Ratio (%) |
| ---------------- | ---------------------:|
| Deflate          | 46.95                 |
| GZip             | 41.89                 |


## Example Usage
The following example demonstrates usage as is a small unit test.

```
#!csharp

#region Usings

    using System;
    using System.IO.Compression;

    using NUnit.Framework;

#endregion

#region Constants

	/// <summary>
	///     The test string.
	/// </summary>
	private const string TestString = "My test string";

#endregion

#region Public Methods and Operators
	[Test]
	public void CompressAndUncompress_StringUsingDeflateOptimal_ExpectMatch()
	{
		// arrange

		// act
		var compress = TestString.Compress();
		var decompress = compress.Decompress();

		// assert
		Assert.Greater(TestString.Length, compress.Length);
		StringAssert.AreEqualIgnoringCase(TestString, decompress);
		Console.WriteLine(@"Starting string data size={0} bytes", TestString.Length);
		Console.WriteLine(@"Compressed data size={0} bytes", compress.Length);
		Console.WriteLine(@"Uncompressed data size={0} bytes", decompress.Length);
		Console.WriteLine(
			@"Compression Ratio={0}%",
			CalculateCompressionRatioPercent(TestString.Length, compress.Length).ToString("F"));
	}
#endregion

```

